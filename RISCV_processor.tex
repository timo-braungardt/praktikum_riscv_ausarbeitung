\documentclass[a4paper,headings=standardclasses,bibliography=totoc]{scrartcl}
\usepackage[hidelinks]{hyperref}    % makes the PDF table of contents
\usepackage{booktabs}               % for better tables
\usepackage{cite}                   % for citation with bibtex 
\usepackage[printonlyused]{acronym} % for list of acronyms
\usepackage{graphicx}               % for pictures
\usepackage{tikz}                   % for drawings
%\usepackage[nottoc]{tocbibind}   % for references in toc       % use bibliography=totoc for scrartcl

\graphicspath{ {./pictures/} }

\begin{document}

\title{Implementing a RISC-V Processor on an FPGA}
\author{Timo Lucas Braungardt\\4120230}

\maketitle
\thispagestyle{empty}       % no page numbering on first page

% =================================================================================
\begin{abstract}
    The objective of this practical is the implementation of a basic softcore RISC-V processor on an \ac{FPGA}.
    For simplicity, we support the RV32I ISA with integer operations, which does not provide floating point operations or multiplication.
    The implementation is further adapted to the architecture of the Xilinx Artix~7 to utilize the given blocks to implement different parts of the processor.
\end{abstract}

\vspace*{\fill}

\tableofcontents

% =================================================================================
\newpage
\medskip
\noindent
Ich versichere, dass ich diese Arbeit selbstständig verfasst und nur die angegebenen Quellen und Hilfsmittel verwendet habe und die Grundsätze und Empfehlungen ``Verantwortung in der Wissenschaft'' der Universität Heidelberg beachtet wurden. 

\vspace*{50pt}
\noindent
\underline{\phantom{mmmmmmmmmmmmmmmmmmmm}}

\medskip
\noindent 
Abgabedatum: \today
\newpage

% =================================================================================
\section{The RISC-V Instruction Set Architecture}
The RISC-V standard defines two base \acp{ISA}.
The I variant has the full 32-entry register file, whereas the E variant only has 16 registers.
Both only implement integer operations.
Extensions for these base \acp{ISA} expand their capabilities:

\begin{itemize}
    \item \textbf{M} for multiplication and division
    \item \textbf{A} for atomic operations
    \item \textbf{F} for floating point support
    \item \textbf{D} for double-precision floating-point support
    \item \textbf{C} for compressed instructions
    \item \textbf{Zicsr} for access to the status registers
\end{itemize}
Further extensions enable better multi-threading, consistency models, or SIMD instructions.
Apart from the 32-bit variant, there are also 64-bit and 128-bit versions, which describe the width of the registers.
Others are reserved but not yet fully defined \cite{risc_isa}.

The privileged \ac{ISA} only has to be supported, if the processor is used in conjunction with an operating system.
Since we want a bare metal processor, this is not necessary and this would exceed the level of an entry practical.
With a privileged mode, the programs can call system functions and are restricted to their own memory space.

\subsection{Basics of the Instruction Set Architecture}
RISC-V is a \ac{RISC} architecture.
These architectures feature instructions which are all the same size.
This simplifies the decoding of the instruction which makes a higher clock rate possible.
Complex functions are not natively supported by the architecture.
Those are implemented by the compiler with the help of concatenation of multiple instructions.
And all computations happen in the register file; operands can't come from memory \cite{reduced_instruction_set_computers}.

The \ac{ISA} is quite small with only 40 instructions for the full 32I \ac{ISA}.
The instructions are grouped into 6 types.
These perform similar operations on the data and use the same components of the processor.
The arithmetic operations are either R-type or I-type.
R-type has both its operands stored in the register file, and I-type has one operand encoded in the instruction.
Loading data from the memory into the register file uses the I-type operation, too.
The S-type operations store the data from a register into the memory. 
No other instruction operates on the contents of the memory, making the \ac{ISA} a load-store architecture.
This means that for data to be processed, it has to be loaded into the registers, and only then can they be manipulated.
The J-type operations are jump instructions; the B-type operations are conditional jumps or branches.
The U-type operation is used to write to the upper bits of a register.

If we take a look at the binary encoding of the instructions in Fig. \ref{fig:instruction_sections}, we can identify that the segments are always placed in the same location.
The opcode tells the processor which type of instruction it is and therefore, how the bits are grouped into register file addresses, immediate fields and control bits.
The placement of the immediate is different for different instruction formats, but certain parts are located at the same bit positions.
Fig. \ref{fig:immediate_sections} highlights this.
When we look at the same bits in different immediate types, they have at most three different sources.
For example, the immediate bits 10 to 5 are from the instruction bits 30 to 25 or zero. 
Or bit 31 of the instruction always contains the \ac{MSB} of the immediate, which is the sign bit.
With the placement of the immediate segments, the amount of multiplexers needed to decode the instruction is minimized.

\begin{figure}[ht]
    \centering
    \includegraphics[width=12cm, keepaspectratio]{instruction_types.png}
    \caption{The sections of the RISC-V instruction types \cite[p.~16]{risc_isa}.}
    \label{fig:instruction_sections}
\end{figure}

\begin{figure}[ht]
    \centering
    \includegraphics[width=12cm, keepaspectratio]{immediate_types.png}
    \caption{The immediates of the RISC-V instruction types \cite[p.~17]{risc_isa}.}
    \label{fig:immediate_sections}
\end{figure}

Further Instructions are the ecall and ebreak instructions, which switch control to the privileged execution environment.
The fence instruction orders memory operations.
With the RISC-V memory consistency model, memory operations may seem reordered only with multithreading.
From the perspective of the thread, the instructions are always executed in order \cite[p.~83]{risc_isa}.
We don't have a privileged execution environment to call and no multithreading, therefore these instructions don't do anything on our CPU\footnotemark{}.

\footnotetext{
    \parbox[t]{\linewidth}{     % nessessary, because otherwise the footnote would span two pages
    To be precise, it doesn't affect the state of the CPU.
    These instructions make an addition and write back to the rd address due to the default values of the case statements.
    In ecall and ebreak the rd is always set to x0, therefore the write is discarded.
    For fence, we suppress the write enable.
    For fence, the rd address is reserved for future use and should always be zeroed \cite[p.~26]{risc_isa}.
    Therefore we could save the logic for the suppression but this is not the best practice for the negible logic overhead.}
}


\subsection{Misaligned Memory Access}
On the 32 \ac{ISA} variants, the operands called words are 32-bit wide. 
A word can be further split into half-words (16-bit) and bytes (8-bit).
With the RISC-V address space, every byte is addressable and a word can start at any address.
But our memory is word-addressable and can only load one word per cycle.
Therefore one word in the address space could span two addresses in the memory.
Based on the \ac{EEI}\footnotemark{} the access may be handled, otherwise an exception should be thrown \cite[p.~24]{risc_isa}.
Either additional hardware or software is needed to handle the access.

\footnotetext{
    The Execution Environment executes the program.
    This can be an operating system or in our case just the bare metal CPU.
    The \ac{EEI} specifies how to interact with the environment and what it supports.
    For example, how the variables for a system call are handed over \cite[p.~10]{risc_sbi}.
}

The impact of misaligned memory access is quite big for more complex CPUs.
If the misaligned access is not atomic, the pipeline will stall.
With cached memory, the word could reside on two cache blocks.
If only one block is loaded in the cache, the other has to be fetched; resulting in a substantial delay.

But a misaligned access happens rarely.
The compiler will store the data at the appropriate boundary of the datatype.
If a pointer is cast to another bigger datatype, misalignment happens \cite{unaligned_access}.

Instructions, on the other hand, are always aligned in the address space.
With the 32I \ac{ISA} they are aligned on a 4-byte boundary or a 2-byte boundary with 32C \cite[p.~7]{risc_isa}.


% =================================================================================
\section{The Architecture}
Each instruction is processed in multiple substeps.
Those are \emph{instruction fetch}, \emph{register read}, \emph{execute}, and \emph{register write}.
The instructions utilize the different components of the processor, which are shown in Fig. \ref{fig:riscv_architecture}.
The \ac{PC} tracks which instruction is executed and needs to be fetched from the main memory.
The appropriate registers are selected from the register file, and the data is fed into other components.
Computations are made with the \ac{ALU} and the branch decides, if the next instruction is fetched or if the \ac{PC} should jump to another address.

However, not every instruction uses the components in the same manner.
The R-type instruction reads the values from the register file, operates on them with the ALU, and stores them back in the register file.
Whereas the S-type instruction reads a base address from a register and adds an immediate offset with the ALU. 
Then the data from the second register is stored in memory.
The components are simple, but connecting them in the required way based on the instruction type is complex.

In the first approach, the output of each component was stored in clocked registers.
This had the benefit, that the Verilog code was an easy-to-read case statement.
The inputs of the different components would only change on the given clock, therefore less unnecessary switching happens which lowers the dynamic power requirements of the \ac{FPGA}.
But in the end, there were too many registers in the path, and the processor required two additional clock cycles for each instruction.
Because of the additional clock cycles, combinatoric logic was used.
At first, this used more \acp{LUT}.
Because the different components are often connected in the same way, the logic was optimized with the most used connection as the default value in the case statement.
With this optimization, the \ac{LUT} overhead disappeared, and the required \acp{FF} lowered by a factor of 4 from the original sequential implementation.
The dynamic power wasn't a problem in the first place.

To fetch the next instruction, the \ac{PC} has to increment its value and then the memory loads the next address.
On an \ac{FPGA} this requires two clock cycles because the output of the memory has a register.
A workaround is to use the pre-incremented value of the \ac{PC} for the instruction fetch.
Then the instruction will be stored in the registers with the same clock as the increment of the \ac{PC}.
This has the drawback, that we have to initialize the \ac{PC} to its highest value.
The pre-incremented address has an overflow and address 0 is loaded with the first clock.
Or we always pad our program binary with one no-operation instruction.
Another workaround is to set the memory to the negative edge of the clock.
Then the instruction is loaded before the next positive edge.
To keep the code easy to understand, it was decided to keep the additional clock cycle for the \ac{PC} increment.

\begin{figure}[ht]
    \centering
    \input{tikz/architecture.tikz}
    \caption{Components of the RISC-V Core.}
    \label{fig:riscv_architecture}
\end{figure}

\subsection{Traps and Exceptions}
The execution of an instruction can fail as an example due to a misalignment of instructions in the memory or a misaligned memory access.
An exception in the execution has to be handled, otherwise, the running program will have undefined behaviour.
To handle the exceptions, the processor stops the next instruction to be loaded and switches to a higher privilege level.
The \ac{PC} jumps to a routine to handle the trap and memorizes the instruction, which triggered the trap.
For the program, this whole procedure is transparent, meaning it will run like nothing happened.
But the handling of the trap can take many cycles, therefore it should be avoided.

To handle traps and exceptions, there are 4096 additional \acp{CSR}, but most of them are empty.
To access these registers, additional instructions are needed, which are defined in the Zicsr extension.
The program runs in the user mode, which doesn't have access to all registers.
If a trap occurs, the processor switches to machine mode.

The traps are used for exceptions and interrupts, as well as system calls.
In this implementation, only the trap for the misaligned memory operations is implemented with the minimal amount of \acp{CSR}.
Additionally, privilege levels are not implemented, therefore the program can change all \acp{CSR}.
The registers needed for handling the memory misalignment are:
\begin{itemize}
    \item \ac{mepc} stores the address of the faulting instruction.
    \item \ac{mcause} stores the type of trap.
    \item \ac{mtval} stores the accessed memory address. 
    \item \ac{mtvec} points to the start of the trap handler.
\end{itemize}

The processor notices a misaligned memory access and signals a trap.
\texttt{mepc}, \texttt{mcause} and \texttt{mtval} are set to the appropriate values.
Instead of the next instruction, the address of \texttt{mtvec} is loaded into \ac{PC}.
In the trap vector, the type of trap is checked and the appropriate handler is executed.
For obvious security reasons, \texttt{mtvec} should only be writable in machine mode and never in user mode.
After the trap is resolved, we want to return to the normal program.
The register \texttt{mepc} holds the address of the instruction, which caused the trap.
This address is incremented by four and stored back into \texttt{mepc}.
With the texttt{mret} instruction, \texttt{mepc} is loaded into \ac{PC}, the privilege level is lowered and normal execution continues.

Because three registers have to be written at the same time during trap signalling, a \ac{BRAM} can't be used.

% =================================================================================
\section{Using Artix 7 Specific Components}
The \ac{FPGA} is not only composed of \acp{LUT} and \acp{FF} but implements more complex functions as dedicated blocks.
We can use the \ac{BRAM} and logic units called \ac{DSP}.
The synthesis tool automatically infers these blocks, if the code follows some design guidelines, for example, the width of ports or if the ports are synchronous.
Otherwise, we can give it some hints or instantiate the blocks as primitives.
If the use case of the block is complex, this is necessary.
Because of the complexity of the blocks, the manual instantiation in the code is labour-intensive.

In the architecture, the memory can be put into \ac{BRAM}.
The register file can be put into \ac{BRAM} as well because the blocks from the 7-Series have native support for two read ports \cite{fpga_memory}.
Otherwise, the synthesis puts the register file into \ac{DRAM} which is mapped to the \acp{LUT}.
This is possible because the memory of the \acp{LUT} can be reconfigured during runtime.
Otherwise, the \acp{FF} can be used, too.
The latter is rather costly because we need 1kb memory, which occupies 1024 FFs.

\subsection{Implementing the Arithmetic Logic Unit}

The \ac{DSP} blocks are quite versatile, but one block can't implement all operations of the RISC-V ALU.
The synthesis only uses \ac{DSP} Blocks for the ALU if a certain coding style is obeyed, but then only uses it for addition and subtraction.

As shown in Fig. \ref{fig:dsp_overview}, the \ac{DSP} has a 25x18-bit multiplier with a pre-adder.
The logic after the multiplier can be used for addition, subtraction, and some logic operations.
Because our data is always 32 bits wide, we have to disable the multiplier and concatenate the two inputs A and B to get to our desired width.
The input C is wide enough.
Now we can implement the addition, subtraction, and bitwise logic functions.

\begin{figure}[ht]
    \centering
    \includegraphics[width=8cm, keepaspectratio]{dsp_overview.png}
    \caption{Basic DSP Functionality \cite[p.~9]{fpga_dsp}}
    \label{fig:dsp_overview}
\end{figure}

For the logic functions, \emph{set less than} for signed and unsigned data, we can utilize the subtraction.
If we calculate $a-b$, the \ac{MSB} is 1 if b was greater than a, because the result is negative.
Now we can use the pattern detection of the \ac{DSP} to ignore the lower bits and check the \ac{MSB}.
The result of the pattern detection has a dedicated output.

If the comparison should be unsigned, the inverted carry-out of the DSP is used to determine the result.
To understand the comparison, we just need to look at the \acp{MSB} as shown in Fig. \ref{fig:carry_compare}.
If the \acp{MSB} are equal, one bit gets flipped because of the 2-complement.
Now with the bits 1 and 0, the carry-in is propagated to the carry-out.
This means that the comparison is dependent on the prior bits.
If the \acp{MSB} are unequal, they are equalized with the 2-complement.
If the combination is 00, the carry is terminated.
This combination means that b is bigger than a because it had a 1 at the \ac{MSB}.
If the combination is 11, a carry is generated because now a is bigger than b.
The function checks if a and b are equal, too.
If they are equal, b gets inverted and all bits propagate the carry.
The $+1$ of the 2-complement is generated with the carry-in of the adder, which now ripples through to the carry-out.
Therefore, we get the function $a \geq b$, which we invert to get the required $a < b$.

All these operations can be implemented with one \ac{DSP} block.

\begin{figure}[ht]
    \centering
    \input{tikz/carry_compare.tikz}
    \caption{Unsigned comparison with the MSBs on the left and the actual computation on the right. The b-bits get flipped due to the 2-complement.}
    \label{fig:carry_compare}
\end{figure}

\subsection{Implementing the Shift Operation}
The shift operation is the most resource-intensive operation.
One data element can be shifted by 31 places, therefore needing 32 32-input \acp{MUX}.
First, we only focus on the left shift.
This can be implemented by a multiplication of the data element with a one-hot encoded factor.
The factor has a one at the bit, at which the \ac{LSB} of the data element should start.
As shown in Fig. \ref{fig:shift_multiplicator}, a shift by zero has a one at the most right bit (bit~0), and a shift by three has a one at bit 3.
But as mentioned before, one \ac{DSP} block can only multiply 25x18, not the required 32x32.
The \ac{DSP} blocks can be concatenated, requiring 3 blocks for the left shift and 4 additional blocks for the right shift.

\begin{figure}[ht]
    \centering
    \input{tikz/shift_multiplicator.tikz}
    \caption{Left shifting by multiplication with a one-hot encoded factor.}
    \label{fig:shift_multiplicator}
\end{figure}

A cheaper solution is to use a barrel shifter designed for the 5 series \acp{FPGA} from Xilinx \cite{barrel_shifter}.
We split up the data into four bytes and shift them individually by up to 7 bits with multiplicators.
Then we rearrange the results with \acp{MUX}, achieving a shift by 8, 16, and 24 bits.
As illustrated by Fig. \ref{fig:byte_shift} we concatenate two bytes for the multiplicators and use the upper byte of the result.
The bits of \emph{byte a} are shifted into the lower spots of the result.
In Table \ref{tab:multiplicators_input}, it is shown that the multiplicator inputs for the left shift and how the results need to be reordered by the \acp{MUX} in Table \ref{tab:multiplexer_input}.

\begin{figure}[ht]
    \centering
    \input{tikz/byte_shift.tikz}
    \caption{Left shifting of one byte for the barrel shifter.}
    \label{fig:byte_shift}
\end{figure}

However, we also want to shift to the right without much overhead.
If we look at Fig. \ref{fig:byte_shift}, we see that a left shift of seven is similar to a right shift of one.
To get a right shift we only have to mirror the factor and enlarge it by one.
This is because a right shift of zero is equal to a left shift of 8.
The inputs of the multiplicators need to change, too.
In Fig. \ref{fig:byte_shift} we left shifted \emph{byte b}, but right shifted in relation to \emph{byte a}.
In Table \ref{tab:multiplicators_input} we see, that the multiplications for the right shift are already made by other multiplicators.
We just need to route the results with our \acp{MUX} differently.
Additionally, we can get the left shift result of mult\_0 by using the lower byte from the shift of \emph{byte 1 - byte 0}.
Therefore, if we only compute the right shift combinations, we get the left shift combinations for free.

\begin{table}[ht]
    \centering
    \caption{Input of the multiplicators}
    \label{tab:multiplicators_input}
    \begin{tabular}{llr}
        \toprule
        input for& \multicolumn{1}{c}{left shift} & \multicolumn{1}{c}{right shift} \\ \midrule
        mult\_3  & byte 3 - byte 2 & sign   - byte 3 \\
        mult\_2  & byte 2 - byte 1 & byte 3 - byte 2 \\
        mult\_1  & byte 1 - byte 0 & byte 2 - byte 1 \\
        mult\_0  & byte 0 - zero   & byte 1 - byte 0
    \end{tabular}
\end{table}

\begin{table}[ht]
    \centering
    \caption{Output of the multiplexers}
    \label{tab:multiplexer_input}
    \begin{tabular}{l ccccccc}
        \toprule
         & & \multicolumn{3}{c}{left shift} & \multicolumn{3}{c}{right shift} \\ 
        output of & 0         & 8           & 16          & 24          & 8         & 16        & 24    \\ \midrule
        mux\_3  & mult\_3     & mult\_2     & mult\_1     & mult\_0     & sign      & sign      & sign  \\
        mux\_2  & mult\_2     & mult\_1     & mult\_0     & zero        & mult\_3   & sign      & sign  \\
        mux\_1  & mult\_1     & mult\_0     & zero        & zero        & mult\_2   & mult\_3   & sign  \\
        mux\_0  & mult\_0     & zero        & zero        & zero        & mult\_1   & mult\_2   & mult\_3
    \end{tabular}
\end{table}

Table \ref{tab:shifter_resources} shows that the barrel shifter uses fewer \ac{DSP} blocks than the 64x32 multiplicator version.
If it makes sense to switch from the purely \ac{LUT} based version to the barrel shifter depends on the availability of the resources.
If we need our \acp{DSP} for other functions, it doesn't make sense to use them for shifting, if \acp{LUT} are still available and vice versa.

\begin{table}[ht]
    \centering
    \caption{Resources for the shifters}
    \label{tab:shifter_resources}
    \begin{tabular}{l r r r}
        \toprule
        Type & LUTs & FFs & DPSs \\ \midrule
        no DSP & 165 & 0 & 0 \\
        64x32 Multiplicator & 122 & 17 & 7 \\
        Barrel & 78 & 0 & 4
    \end{tabular}
\end{table}

% =================================================================================
\section{Conclusion and Outlook}
In this practical the successful implementation of a basic RISC-V processor by an undergraduate student was shown.
The architecture was implemented without any prior knowledge of processor design.
The misaligned memory access was solved in hardware as well as in software.
Both implementations are reasonable to use, based on the design requirements.
The \ac{ALU} was implemented in two versions.
One using only \acp{LUT} and one version using \ac{DSP} blocks to free up some \acp{LUT}.
The \ac{DSP} version requires five \ac{DSP} blocks.
The shifts were implemented with a barrel shifter design from Xilinx, using four \ac{DSP} blocks.
The barrel shifter uses multiplication to shift the operand to the left and right.
Only one \ac{DSP} block was needed for the other functions.

With more time we would implement the system call functionality of the CPU, since some parts of the privileged \ac{ISA} are already implemented.
Since this was the first CPU design, other implementations could be studied to gain some inspiration for improvement.
An in-depth analysis of the critical path would be useful to identify shortcomings of the architecture.

% =================================================================================
\clearpage
\section*{List of Abbreviations}
\addcontentsline{toc}{section}{List of Abbreviations}

\begin{acronym}[mcause]     % in the brackets, the longest acronym is needed to get the spacing right
    \acro{ALU}{Arithmetic Logic Unit}
    \acro{BRAM}{Block RAM}
    \acro{CSR}{Control and Status Register}
    \acro{DRAM}{Distributed RAM}
    \acro{DSP}{Digital Signal Processor}
    \acro{EEI}{Execution Environment Interface}
    \acro{FF}{Flip Flop}
    \acro{FPGA}{Field Programmable Gate Array}
    \acro{ISA}{Instruction Set Architecture}
    \acro{LSB}{least signigicant bit}
    \acro{LUT}{Lookup Table}
    \acro{mcause}{Machine Cause Register}
    \acro{mepc}{Machine Exception Program Counter}
    \acro{MSB}{most significant bit}
    \acro{mtval}{Machine Trap Value Register}
    \acro{mtvec}{Machine Trap-Vector Base-Address Register}
    \acro{MUX}{Multiplexer}
    \acro{PC}{Program Counter}
    \acro{RISC}{Reduced Instruction Set Computer}
\end{acronym}

\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,refs}    % Entries are in the refs.bib file

\end{document}
